var {LiveTile,registerTile,getLibraries} = require("@ombiel/exlib-livetile-tools");
var screenLink = require("@ombiel/aek-lib/screen-link");
var publicationCSS = require("../../css/publication");
var AekStorage = require("@ombiel/aek-lib/storage");
var storage = new AekStorage("uon-publication-lt");
var _ = require("-aek/utils");

var $;
var libReady = getLibraries(["jquery"]).then((libs)=>{
  [$] = libs;
});

class PublicationLiveTile extends LiveTile {
  onReady() {

    this.setOptions();

    libReady.then(()=>{
      this.fetchOnVisibility = (v)=>{
         if (v === "visible") {
            this.fetchData();
          }
      };
      this.renderImmediately = this.render.bind(this);

      // Debounce render so it doesn't get triggered more than once in a 1 sec period
      this.render = _.throttle(this.render.bind(this),1000);

      this.fetchData();

      var cacheData = storage.get("cacheData");
      if(cacheData) {
        // Use cached data
        this.data = cacheData;
        this.render();
      }
      else {
        // If there is no cache show original face
        this.render();
      }
    });
  }

  fetchData(){
    this.setOptions();
    var url = screenLink("uon-publication-lt/data");
    var refetchPeriod;
    this.ajax({url: `/campusm/sso${url}`})
    .then((data)=>{
      if(data){
        if(data !== "SSO Host Mismatch"){
          this.offPageVisibility(this.fetchOnVisibility);
          this.data = data;
          refetchPeriod = this.refetchPeriod;
          storage.set("cacheData",data);
          this.render();
        }else{
          this.onPageVisibility(this.fetchOnVisibility);
          this.showOriginalFace();
          this.ssoError = true;
          refetchPeriod = 30000;
        }
      }else{
        this.offPageVisibility(this.fetchOnVisibility);
        this.count = 0;
        refetchPeriod = this.refetchPeriod;
        storage.set("cacheData",0);
        this.render();
      }
      this.timer(refetchPeriod,this.fetchData.bind(this));
    });
  }

  render(){
    if(this.data){
      let data = this.data;

      if(_.isArray(data) && data.length > 0){
      this.count = data.length;
    }else if(!_.isArray(data) && !_.isEmpty(data) && data.length > 0){
      this.count = 1;
    }else{
      this.count = 0;
    }


      // Elements
      var faceContainer = document.createElement("div");
      var number = "0";
      // Tile Attributes
      var tileAttributes = this.getTileAttributes();

      // Assign CSS classes to Elements
      $(faceContainer).addClass(publicationCSS.publicationFaceContainer);

      // Tile background image set in runserver.yaml
      if(tileAttributes.img || tileAttributes.image){
        var image = tileAttributes.image || tileAttributes.img;
        $(faceContainer).css("background-image",'url(' + image + ')');
      }

      this.front = true;

      if(parseInt(this.count) > 99) {
        number = ">99";
      }else if(parseInt(this.count) > 0){
        number = String(this.count);
      }else{
        number = "0";
      }

      this.detailsTile = this.flipFace(faceContainer,{scale:false});
      var badge = this.detailsTile.setBadge(number, false);
      var style = badge.DOMElement.style;

      if(this.count > 0) {
        style.backgroundColor = "#cf1a23";
      }

    }else{
      console.warn("No Data"); //eslint-disable-line no-console
      this.showOriginalFace();
    }
  }

  setOptions(){
    var tileAttributes = this.getTileAttributes();
    this.initPause = parseInt(tileAttributes.initPause) || 3000;
    this.refetchPeriod = parseInt(tileAttributes.refetchPeriod) || 3600000;
  }
}


registerTile(PublicationLiveTile,"publicationLiveTile");
